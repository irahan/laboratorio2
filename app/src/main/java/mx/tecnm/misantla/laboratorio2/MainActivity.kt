package mx.tecnm.misantla.laboratorio2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imgGeneracion.setImageResource(R.drawable.picture)

        btnVerificar.setOnClickListener {

            val anio =edtAnio.text.toString().toInt()
          when(anio) {
                in 1930..1948 -> {

                    tvResultadoGeneracion.text ="SILENT GENERATION "
                    imgGeneracion.setImageResource(R.drawable.gsg)
                }
                in 1949..1968 -> {

                    tvResultadoGeneracion.text = "BABY BOOM"
                            imgGeneracion.setImageResource(R.drawable.gbb)
                }
                in 1969..1980 -> {

                    tvResultadoGeneracion.text ="GENERACION X"
                            imgGeneracion.setImageResource(R.drawable.gx)
                }
                in 1981..1993 -> {

                    tvResultadoGeneracion.text ="GENERACION Y"
                            imgGeneracion.setImageResource(R.drawable.gy)
                }
                in 1994..2010 -> {

                    tvResultadoGeneracion.text = "GENERACION Z"
                            imgGeneracion.setImageResource(R.drawable.gz)
                }
                else -> {
                    tvResultadoGeneracion.text ="NO DEFINIDO"
                }
            }

        }

    }
}